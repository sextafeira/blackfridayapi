package com.eventos.utils;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ValidadorGenericoTest {
	
	@Test(expected = IllegalArgumentException.class)
	public void data_nao_deve_ser_nula(){
		ValidadorGenerico.checkdata(null);	
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void data_nao_deve_ser_vazia(){
		ValidadorGenerico.checkdata("");	
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void ano_nao_pode_ter_formato_invalido() {
		ValidadorGenerico.checkdata("31/07/20000");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void ano_nao_pode_ser_maior_que_2999() {
		ValidadorGenerico.checkdata("31/07/3000");
	}
	
	@Test
	public void data_deve_ser_valida(){
		assertTrue(ValidadorGenerico.checkdata("29/07/2017"));
	}
	@Test
	public void data_deve_ser_valida_em_meses_com_31_dias(){
		assertTrue(ValidadorGenerico.checkdata("31/12/2017"));
	}
	
	@Test
	public void data_deve_ser_valida_em_meses_com_29_dias(){
		assertTrue(ValidadorGenerico.checkdata("29/02/2017"));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void data_nao_deve_ser_valida_em_meses_com_maximo_de_29_dias(){
		ValidadorGenerico.checkdata("30/02/2017");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void data_nao_deve_ser_valida_em_mes_maior_que_12(){
		ValidadorGenerico.checkdata("30/13/2017");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void ano_nao_deve_ser_menor_que_1000(){
		ValidadorGenerico.checkdata("30/12/0000");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void data_nao_pode_ter_formato_separado_por_tracos() {
		ValidadorGenerico.checkdata("31-07-3000");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void url_nao_deve_ser_nula() {
		ValidadorGenerico.checkUrl(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void url_nao_deve_ser_vazia() {
		ValidadorGenerico.checkUrl("");
	}
	
	@Test
	public void url_pode_deve_formato_valido() {
		assertTrue(ValidadorGenerico.checkUrl("http://www.seusite.com/#sthash.yd2rb1Wc.dpbs"));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void url_nao_pode_ter_formato_invalido() {
		ValidadorGenerico.checkUrl("login.logout");
	}
}
