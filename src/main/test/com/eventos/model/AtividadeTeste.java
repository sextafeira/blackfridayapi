package com.eventos.model;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.eventos.model.activities.AtividadeRegistravelSimples;
import com.eventos.model.activities.Responsavel;
import com.eventos.model.local.Local;
import com.eventos.model.local.LocalComposto;
import com.eventos.model.local.LocalSimples;
import com.eventos.model.local.TipoLocal;
import com.eventos.model.users.Usuario;
import com.eventos.utils.DateTimeRange;


public class AtividadeTeste {
	Usuario usuario;
	Responsavel responsavel;
	ArrayList <Responsavel> responsaveis;
	
	
	AtividadeRegistravelSimples atividadeRegistravel1;
	AtividadeRegistravelSimples atividadeRegistravel2;
	AtividadeRegistravelSimples atividadeRegistravel3;
	AtividadeRegistravelSimples atividadeRegistravel4;
	
	LocalDateTime oitoHorasETrintaMinutos = LocalDateTime.of(2017, 01, 01, 8, 30);
	LocalDateTime dezHorasETrintaMinutos = LocalDateTime.of(2017, 01, 01, 10, 30);
	LocalDateTime dozeHorasETrintaMinutos = LocalDateTime.of(2017, 01, 01, 12, 30);
	LocalDateTime catorzeHorasETrintaMinutos = LocalDateTime.of(2017, 01, 01, 14, 30);
	LocalDateTime dezesseisHorasETrintaMinutos = LocalDateTime.of(2017, 01, 01, 16, 30);
	LocalComposto localComposto = LocalComposto.getLocalCompostoPadrao();
	LocalSimples institutoFederal = Local.getLocalPadrao();
	LocalSimples universidadeFederal = Local.getLocalPadrao();
	DateTimeRange escopoDeTempo08as10 = new DateTimeRange(oitoHorasETrintaMinutos, dezHorasETrintaMinutos);
	DateTimeRange escopoDeTempo10as12 = new DateTimeRange(dezHorasETrintaMinutos, dozeHorasETrintaMinutos);
	@Before
	public void setUp() {
		localComposto.setNome("Conjunto de Salas");
		usuario = new Usuario("teste@email.com","1234");
		usuario.setNomeCompleto("Usuário de Teste");
		
		responsavel = new Responsavel();
		responsavel.setNome("usuario responsavel");
		responsavel.setDescricao("este usuario ministrará as paradinhas na atividade");
		
		responsaveis = new ArrayList<Responsavel>();
		responsaveis.add(responsavel);
		
		
		institutoFederal.setName("instituto federal de educação, ciência e tecnologia do piauí");
		institutoFederal.setId(1);
		
		
		
		universidadeFederal.setName("universidade federal do piuai");
		universidadeFederal.setId(2);
		
		atividadeRegistravel1 = new AtividadeRegistravelSimples("atividade simples 01");
		atividadeRegistravel2 = new AtividadeRegistravelSimples("atividade simples 02");
		atividadeRegistravel3 = new AtividadeRegistravelSimples("atividade simples 03");
		atividadeRegistravel4 = new AtividadeRegistravelSimples("atividade simples 04");
		
		
		atividadeRegistravel1.setUsuarioCriador(usuario);
		atividadeRegistravel1.setDataInicio(oitoHorasETrintaMinutos);
		atividadeRegistravel1.setDataTermino(dezHorasETrintaMinutos);
		atividadeRegistravel1.setLocal(institutoFederal);
		atividadeRegistravel1.setResponsaveis(responsaveis);
		
		atividadeRegistravel2.setUsuarioCriador(usuario);
		atividadeRegistravel2.setDataInicio(dozeHorasETrintaMinutos);
		atividadeRegistravel2.setDataTermino(catorzeHorasETrintaMinutos);
		atividadeRegistravel2.setLocal(institutoFederal);
		atividadeRegistravel2.setResponsaveis(responsaveis);
		atividadeRegistravel2.setPreco(15.0);
		
		atividadeRegistravel3.setUsuarioCriador(usuario);
		atividadeRegistravel3.setDataInicio(catorzeHorasETrintaMinutos);
		atividadeRegistravel3.setDataTermino(dezesseisHorasETrintaMinutos);
		atividadeRegistravel3.setLocal(institutoFederal);
		atividadeRegistravel3.setResponsaveis(responsaveis);
		
		atividadeRegistravel4.setUsuarioCriador(usuario);
		atividadeRegistravel4.setDataInicio(catorzeHorasETrintaMinutos);
		atividadeRegistravel4.setDataTermino(dezesseisHorasETrintaMinutos);
		atividadeRegistravel4.setLocal(universidadeFederal);
		atividadeRegistravel4.setResponsaveis(responsaveis);
		
		localComposto.addLocalSimples(institutoFederal);
		
		universidadeFederal = new LocalSimples("HEHEHE", 20, TipoLocal.AUDITORIO);
	}
	
	@Test
	public void atividade_deve_ter_um_criador() {
		assertEquals(usuario, atividadeRegistravel1.getUsuarioCriador());
	}
	
	@Test
	public void dois_Eventos_Nao_Podem_Ocorrer_Simultaneamente_Num_Mesmo_Espaco_Fisico() {
		assertEquals(false, atividadeRegistravel1.hasConflictWith(atividadeRegistravel2));
	}
	
	@Test
	public void deve_ocorrer_colisao_de_dois_eventos_ocorrendo_simultaneamente_Num_Mesmo_Espaco_Fisico() {
		assertEquals(true, atividadeRegistravel1.hasConflictWith(atividadeRegistravel1));
	}
	
	@Test
	public void dois_eventos_podem_ocorrer_simultaneamente_em_locais_diferentes() {
		assertEquals(false, atividadeRegistravel3.hasConflictWith(atividadeRegistravel4));
	}
	
	@Test
	public void lista_de_responsaveis_deve_ter_apenas_um_responsavel() {
		assertEquals(1, atividadeRegistravel1.getResponsaveis().size());
	}

	@Test 
	public void atividade_deve_ter_preco_inicial_igaul_a_zero() {
		assertEquals( new Double(0) , atividadeRegistravel1.getPreco());
	}
	@Test 
	public void atividade_deve_ter_retornar_preco_correto() {
		assertEquals( new Double(15) , atividadeRegistravel2.getPreco());
	}
}
