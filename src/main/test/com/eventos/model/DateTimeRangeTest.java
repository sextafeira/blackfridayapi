package com.eventos.model;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;

import com.eventos.utils.DateTimeRange;

public class DateTimeRangeTest {
	LocalDateTime data01;
	LocalDateTime data02;
	LocalDateTime data03;
	LocalDateTime data04;
	LocalDateTime data05;
	DateTimeRange dateTimeRange01;
	DateTimeRange dateTimeRange02;
	DateTimeRange dateTimeRange03;
	
	@Before
	public void setUp() {
		
		data01 = LocalDateTime.of(2017, 9, 01, 00, 00, 00);
		data02 = LocalDateTime.of(2017, 9, 01, 02, 30, 00);
		data03 = LocalDateTime.of(2017, 9, 01, 02, 29, 00);
		data04 = LocalDateTime.of(2017, 9, 01, 06, 00, 00);
		data05 = LocalDateTime.of(2017, 9, 01, 8, 00, 00);
		
	}
	
	@Test
	public void datetimerange_endDate_shouldnt_be_higher_or_equal_than_startDate() {
		dateTimeRange01 = new DateTimeRange(data01, data02);
		assertEquals(-1, dateTimeRange01.getDataInicio().compareTo(dateTimeRange01.getDataTermino()));
	}
	
	@Test
	public void datetimerange_shouldnt_colide_with_anothe_datetimerange() {
		dateTimeRange02 = new DateTimeRange(data01,data02);
		dateTimeRange03 = new DateTimeRange(data04,data05);
		
		assertEquals(false, dateTimeRange02.hasConflict(dateTimeRange03));
		
	}
	@Test
	public void datetimerange_should_colide_with_anothe_datetimerange() {
		dateTimeRange02 = new DateTimeRange(data01,data02);
		dateTimeRange03 = new DateTimeRange(data03,data04);
		
		assertEquals(true, dateTimeRange02.hasConflict(dateTimeRange03));
		
	}
	@Test
	public void datetimerange_should_return_correct_hours_interval() {
		dateTimeRange02 = new DateTimeRange(data01,data02);
		
		assertEquals(2, dateTimeRange02.getHoursInterval());
		
	}
	@Test
	public void datetimerange_should_return_correct_minutes_interval() {
		dateTimeRange02 = new DateTimeRange(data01,data02);
		
		assertEquals(30, dateTimeRange02.getMinutesInterval());
		
	}
	

}
