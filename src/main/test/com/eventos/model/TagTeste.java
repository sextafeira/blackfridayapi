package com.eventos.model;

import org.junit.Test;

import com.eventos.model.events.Evento;
import com.eventos.model.events.Tag;
import com.eventos.model.users.Usuario;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;

public class TagTeste {
	
	private Tag tag;
	private ArrayList<Evento> eventos;
	
	@Before
	public void setUp() {
		
		tag = new Tag("biscoito");
		eventos = new ArrayList<>();

		eventos.add(new Evento(
				"Os bichos voadores",
				Usuario.getUsuarioPadrao()
				));
		eventos.add(new Evento(
				"Os bichos roedores",
				Usuario.getUsuarioPadrao()
				));
		eventos.add(new Evento(
				"Os bichos matadores",
				Usuario.getUsuarioPadrao()
				));
		eventos.add(new Evento(
				"Os bichos amadores",
				Usuario.getUsuarioPadrao()
				));
		eventos.add(new Evento(
				"As aves nao voadores",
				Usuario.getUsuarioPadrao()
				));
		eventos.add(new Evento(
				"O ultimo dos eventos",
				Usuario.getUsuarioPadrao()
				));
		
		for (Evento evento : eventos) {
			evento.adicioneUmaTag(tag);
		}
		
		
	}
	
	@Test(expected = RuntimeException.class)
	public void tag_nao_deve_ter_nome_vazio() {
		tag.setNome("");
	}
	
	@Test
	public void tag_deve_mostrar_eventos_que_tenha_seu_nome() {
		for (Evento evento : eventos) {
			assertEquals(evento.getTags().contains(tag), true);
		}
		
	}
		
}
