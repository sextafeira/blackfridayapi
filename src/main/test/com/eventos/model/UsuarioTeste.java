package com.eventos.model;

import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import com.eventos.model.users.Usuario;


public class UsuarioTeste {
	private Usuario usuario;
	
	@Before
	public void setUp() {
		usuario = new Usuario("daniel@gmail.com", "daniel");
	}
	@Test(expected = RuntimeException.class)
	public void usuario_nao_deve_ter_email_nulo() {
		usuario.setEmail(null);
	}
	@Test(expected = RuntimeException.class)
	public void usuario_deve_ter_email_valido() {
		usuario.setEmail("Qualquer coisa");
	}
	@Test(expected = RuntimeException.class)
	public void usuario_nao_deve_ter_senha_nula() {
		usuario.setSenha(null);
	}
	@Test
	public void usuario_deve_ter_lista_de_eventos_administrados_vazia() {
		assertEquals(usuario.getEventosAdministrados().equals(new ArrayList<>()), true);
	}
	@Test
	public void usuario_deve_ter_lista_de_inscricoes_vazia() {
		assertEquals(usuario.getInscricoes().equals(new ArrayList<>()), true);
	}
	
}