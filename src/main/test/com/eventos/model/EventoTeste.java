package com.eventos.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import org.junit.Test;
import com.eventos.model.events.Evento;
import com.eventos.model.users.Usuario;
import org.junit.Before;

public class EventoTeste {
	private Evento eventoTeste;
	
	@Before
	public void setUp() {
		eventoTeste = 
			new Evento(
				"Semana dos Testes",
				new Usuario(
					"matt@live.com",
					"321"
						)
				);
	}
	
	@Test(expected = RuntimeException.class)
	public void evento_criado_nao_pode_ter_nome_vazio() {
		eventoTeste.setNome("");
	}
	
	@Test(expected = RuntimeException.class)
	public void evento_criado_nao_pode_ter_nome_nulo() {
		eventoTeste.setNome(null);
	}
	
	@Test(expected = RuntimeException.class)
	public void evento_criado_nao_pode_ter_usuario_criador_nulo() {
		eventoTeste.setUsuarioCriador(null);
	}

	@Test
	public void evento_criado_deve_ter_lista_de_atividades_vazia(){
		assertEquals(eventoTeste.getAtividades().equals(new ArrayList<>()), true);
	}
	@Test
	public void evento_permitir_adicionar_adiministrador() {
		eventoTeste.adicioneAdministrador(new Usuario("alek@gmail.com","12345"));
		assertEquals(eventoTeste.getAdministradores().equals(null), false);
		assertEquals(eventoTeste.getAdministradores().size(),1);
	}
	

}