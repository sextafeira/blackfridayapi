package com.eventos.model;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.Test;

import com.eventos.model.activities.AtividadeRegistravelSimples;
import com.eventos.model.activities.PacoteDeAtividades;
import com.eventos.model.events.EstadoEvento;
import com.eventos.model.events.Evento;
import com.eventos.model.registration.Cupom;
import com.eventos.model.registration.Inscricao;
import com.eventos.model.users.Usuario;

import org.junit.Before;

public class InscricaoTeste {
	Inscricao inscricao;
	Evento evento;
	Usuario usuario;
	PacoteDeAtividades pacote;
	AtividadeRegistravelSimples atividade;
	
	@Before
	public void setUp() {
		usuario = new Usuario("wil@live.com", "8h72nb");
		evento = new Evento("Evento no IFPI",usuario);
		evento.setEstadoEvento(EstadoEvento.ABERTO);
		inscricao = new Inscricao(evento);
		atividade = new AtividadeRegistravelSimples("Atv");
		pacote = new PacoteDeAtividades("PacAts");
	}
	@Test(expected = RuntimeException.class)
	public void incricao_nao_pode_ter_usuario_nulo() {
		usuario = null;
		evento.setUsuarioCriador(usuario);
		inscricao = new Inscricao(evento);
		
	}
	@Test(expected = RuntimeException.class)
	public void inscricao_nao_pode_ter_evento_nulo() {
		evento = null;
		inscricao = new Inscricao(evento);
		
	}
	
	@Test
	public void inscricao_nao_pode_ter_numero_de_atividades_inscritas_zerada() {
		evento.addAtividade("palestra");
		inscricao = new Inscricao(evento);
		assertEquals(inscricao.getAtividades().size(), 1);
	}
	
	@Test
	public void inscricao_deve_ter_numero_de_atividades_do_pacote() {
		pacote.addAtividade(this.atividade);
		inscricao = new Inscricao(pacote);
		assertEquals(inscricao.getAtividades().size(), 1);
	}
	
	
	@Test(expected = RuntimeException.class)
	public void inscricao_nao_pode_deixar_inserir_atividades_que_ja_foram_pagas() {
		evento.addAtividade("palestra");
		inscricao = new Inscricao(evento);
		inscricao.pagar(this.usuario);
		inscricao.adicioneUmaAtividade(atividade);

	}
	@Test
	public void inscricao_por_evento_deve_somar_corretamente_o_preco_das_atividades(){
		evento.addAtividade("atv");
		evento.addAtividade("ats");
		List<AtividadeRegistravelSimples> atividades = evento.getAtividades();
		for(AtividadeRegistravelSimples atividade:atividades)
			atividade.setPreco(200);
		inscricao = new Inscricao(evento);
		assertEquals(inscricao.getValorTotal(), 400 ,0);
	}
	
	@Test
	public void inscricao_por_atividade_deve_somar_corretamente_o_preco_das_atividades(){
		evento.addAtividade("atv");
		evento.addAtividade("ats");
		List<AtividadeRegistravelSimples> atividades = evento.getAtividades();
		for(AtividadeRegistravelSimples atividade:atividades)
			atividade.setPreco(200);
		inscricao = new Inscricao(atividades);
		assertEquals(inscricao.getValorTotal(), 400 ,0);
	}
	
	@Test
	public void inscricao_por_pacote_deve_somar_corretamente_o_preco_das_atividades(){
		AtividadeRegistravelSimples atv = new AtividadeRegistravelSimples("ats");
		pacote.addAtividade(atividade);
		pacote.addAtividade(atv);
		List<AtividadeRegistravelSimples> atividades = pacote.getAtividadeRegistravelSimples();
		for(AtividadeRegistravelSimples atividade: atividades)
			atividade.setPreco(200);
		inscricao = new Inscricao(pacote);
		assertEquals(inscricao.getValorTotal(), 400 ,0);
	}
	
	@Test
	public void inscricao_deve_calcular_corretamente_seu_preco_final_quando_cupom_for_inserido() {
		atividade.setPreco(200);
		evento.addAtividade(atividade);
		Cupom cupom = Cupom.getAutomaticCoupon(50.0, LocalDateTime.of(2017,10,05,0,0), LocalDateTime.of(2017,12,05,0,0));
		inscricao = new Inscricao(evento);
		inscricao.setCupomNormal(cupom);
		assertEquals(inscricao.getValorTotal(),100,0);	
	}
	
	@Test
	public void inscricao_deve_inserir_atividades_do_secundario() {		
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void inscricao_nao_deve_permitir_usuarios_caso_nao_aberta() {
		//
		evento.setEstadoEvento(EstadoEvento.NOVO);
		inscricao.setEvento(evento);
		
		evento.setEstadoEvento(EstadoEvento.TERMINADO);
		inscricao.setEvento(evento);
		//
	}
}