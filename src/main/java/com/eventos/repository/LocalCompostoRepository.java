package com.eventos.repository;

import com.eventos.model.local.LocalComposto;

public interface LocalCompostoRepository extends BaseRepository<LocalComposto, Long>{
	
}
