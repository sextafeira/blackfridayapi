package com.eventos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.eventos.model.events.Evento;

public interface EventoRepository extends BaseRepository<Evento, Long> {
	@Query("SELECT e FROM Evento e WHERE e.usuarioCriador.id = :id")
    public List<Evento> findByUsuarioId(@Param("id") Long id);
	
	@Query("SELECT e FROM Evento e, Usuario u JOIN e.administradores adm JOIN u.eventosAdministrados eadm WHERE u.id = :id")
    public List<Evento> findByUsuarioIdAdministrador(@Param("id") Long id);
}
