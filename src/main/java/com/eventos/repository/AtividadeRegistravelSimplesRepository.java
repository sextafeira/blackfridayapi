package com.eventos.repository;

import com.eventos.model.activities.AtividadeRegistravelSimples;

public interface AtividadeRegistravelSimplesRepository extends BaseRepository<AtividadeRegistravelSimples, Long>{
	
}
