package com.eventos.repository;

import com.eventos.model.registration.Cupom;

public interface CupomRepository extends BaseRepository<Cupom, Long>{
}
