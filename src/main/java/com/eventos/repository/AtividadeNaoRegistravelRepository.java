package com.eventos.repository;

import com.eventos.model.activities.AtividadeNaoRegistravel;

public interface AtividadeNaoRegistravelRepository extends BaseRepository<AtividadeNaoRegistravel, Long>{
	
}
