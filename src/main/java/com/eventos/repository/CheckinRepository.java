package com.eventos.repository;

import com.eventos.model.activities.Checkin;

public interface CheckinRepository extends BaseRepository<Checkin, Long>{

}
