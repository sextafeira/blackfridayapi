package com.eventos.repository;

import com.eventos.model.registration.Pagamento;

public interface PagamentoRepository extends BaseRepository<Pagamento, Long>{
	
}
