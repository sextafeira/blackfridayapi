package com.eventos.repository;

import com.eventos.model.events.Tag;

public interface TagRepository extends BaseRepository<Tag, Long>{
}
