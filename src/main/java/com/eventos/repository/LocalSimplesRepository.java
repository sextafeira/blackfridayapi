package com.eventos.repository;

import com.eventos.model.local.LocalSimples;

public interface LocalSimplesRepository extends BaseRepository<LocalSimples, Long>{
	
}
