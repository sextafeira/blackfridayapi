package com.eventos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.eventos.model.users.Usuario;

public interface UsuarioRepository extends BaseRepository<Usuario, Long> {
	@Query("SELECT u FROM Usuario u WHERE LOWER(u.email) = LOWER(:email) AND LOWER(u.senha) = LOWER(:senha)")
    public List<Usuario> findByEmailAndSenha(@Param("email") String email, @Param("senha") String senha);
}