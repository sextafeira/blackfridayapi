package com.eventos.repository;

import com.eventos.model.registration.Inscricao;

public interface InscricaoRepository extends BaseRepository<Inscricao, Long>{
	
}
