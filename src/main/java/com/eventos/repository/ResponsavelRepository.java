package com.eventos.repository;

import com.eventos.model.activities.Responsavel;

public interface ResponsavelRepository extends BaseRepository<Responsavel, Long>{

}
