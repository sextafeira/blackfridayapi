package com.eventos.repository;

import com.eventos.model.events.Instituicao;

public interface InstituicaoRepository extends BaseRepository<Instituicao, Long>{
	
}
