package com.eventos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.activities.Responsavel;
import com.eventos.repository.BaseRepository;
import com.eventos.repository.ResponsavelRepository;

@RestController
@RequestMapping("/responsaveis")
public class ResponsavelController implements BaseController<Responsavel>{
	@Autowired
	private ResponsavelRepository responsableRepository;

	@Override
	public BaseRepository<Responsavel, Long> getRepository() {
		return this.responsableRepository;
	}
	
}
