package com.eventos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.activities.Trilha;
import com.eventos.repository.BaseRepository;
import com.eventos.repository.TrilhaRepository;

@RestController
@RequestMapping("/trilhas")
public class TrilhaController implements BaseController<Trilha>{
	@Autowired
	private TrilhaRepository trilhaRepository;
	
	@Override
	public BaseRepository<Trilha, Long> getRepository() {
		return this.trilhaRepository;
	}
	
}
