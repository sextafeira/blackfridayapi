package com.eventos.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.users.Usuario;
import com.eventos.model.events.Evento;
import com.eventos.model.users.StatusUsuario;
import com.eventos.repository.BaseRepository;
import com.eventos.repository.EventoRepository;
import com.eventos.repository.UsuarioRepository;
import com.eventos.utils.EmailAdapter;
import com.eventos.utils.NotificationAdapter;

@RestController
@RequestMapping("/usuarios")
@CrossOrigin
public class UsuarioController implements BaseController<Usuario>{
	@Autowired 
	private UsuarioRepository userRepository;
	private EventoRepository  eventRepository;
	private ArrayList<NotificationAdapter> notifiers = new ArrayList<>();
	
	public UsuarioController() {
		notifiers.add(new EmailAdapter());
	}
	
	public void sendNotifications() {
		for (NotificationAdapter notificationAdapter : notifiers) {
			//notificationAdapter.enviarMensagem("10alexsander10@gmail.com", "Novo usuario logado");
			notificationAdapter.enviarMensagem("ddanielsilva661@gmail.com", "Novo usuario logado");
			//notificationAdapter.enviarMensagem("finalwildrimak@gmail.com", "Novo usuario logado");
			//notificationAdapter.enviarMensagem("b10h4z4rd01@gmail.com", "Novo usuario logado");
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<Usuario> login(@RequestBody Usuario user) {
		List<Usuario> users = userRepository.findByEmailAndSenha(user.getEmail(), user.getSenha());
		if(users.size() == 0)
			return new ResponseEntity<Usuario>(HttpStatus.NOT_FOUND);
		else {
			this.sendNotifications();
			return new ResponseEntity<Usuario>(users.get(0), HttpStatus.OK);
		}
	}
	
	@CrossOrigin
	@RequestMapping(value="{id}/confirmacao/{confirmationCode}", method = RequestMethod.GET)
	public ResponseEntity<Usuario> confirm(@PathVariable("confirmationCode") String confirmationCode, @PathVariable("id") Long id) {
		Usuario user = userRepository.findOne(id);
		if(user != null)
			if(user.isValidConfirmationCode(confirmationCode)) {
				user.setStatusUsuario(StatusUsuario.CONFIRMADO);
				user.setCodigoDeConfirmacao("");
				userRepository.save(user);
				return new ResponseEntity<Usuario>(user, HttpStatus.OK);
			}
		return new ResponseEntity<Usuario>(HttpStatus.NOT_FOUND);
	}
	
	@CrossOrigin
	@RequestMapping(value = "/{idUser}/eventos", method = RequestMethod.POST)
	public ResponseEntity<List<Evento>> eventosCriados(@PathVariable("idUser") Long idUser, @RequestBody Usuario user) {
		List<Evento> eventos = eventRepository.findByUsuarioId(idUser);
		if(eventos.size() == 0)
			return new ResponseEntity<List<Evento>>(HttpStatus.NOT_FOUND);
		else {
			this.sendNotifications();
			return new ResponseEntity<List<Evento>>(eventos, HttpStatus.OK);
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/{idUser}/eventosAdministrados", method = RequestMethod.POST)
	public ResponseEntity<List<Evento>> eventosAdministrados(@PathVariable("idUser") Long idUser, @RequestBody Usuario user) {
		List<Evento> eventos = eventRepository.findByUsuarioIdAdministrador(idUser);
		if(eventos.size() == 0)
			return new ResponseEntity<List<Evento>>(HttpStatus.NOT_FOUND);
		else {
			this.sendNotifications();
			return new ResponseEntity<List<Evento>>(eventos, HttpStatus.OK);
		}
	}
	
	public BaseRepository<Usuario, Long> getRepository() {
		return this.userRepository;
	}
}