package com.eventos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.activities.PacoteDeAtividades;
import com.eventos.repository.BaseRepository;
import com.eventos.repository.PacoteDeAtividadesRepository;

@RestController
@RequestMapping("/pacote-de-atividades")
public class PacoteDeAtividadesController  implements BaseController<PacoteDeAtividades>{
	@Autowired
	private PacoteDeAtividadesRepository pacoteDeAtividadesRepository;
	
	@Override
	public BaseRepository<PacoteDeAtividades, Long> getRepository() {
		return this.pacoteDeAtividadesRepository;
	}
}
