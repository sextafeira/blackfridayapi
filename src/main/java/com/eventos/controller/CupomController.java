package com.eventos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.registration.Cupom;
import com.eventos.repository.BaseRepository;
import com.eventos.repository.CupomRepository;


@RestController
@RequestMapping("/cupons")
public class CupomController implements BaseController<Cupom>{
	@Autowired 
	private CupomRepository cupomRepository;
	
	@Override
	public BaseRepository<Cupom, Long> getRepository() {
		return this.cupomRepository;
	}
}
