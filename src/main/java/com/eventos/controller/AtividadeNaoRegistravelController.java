package com.eventos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.activities.AtividadeNaoRegistravel;
import com.eventos.repository.AtividadeNaoRegistravelRepository;
import com.eventos.repository.BaseRepository;

@RestController
@RequestMapping("/atividades-nao-registraveis")
public class AtividadeNaoRegistravelController implements BaseController<AtividadeNaoRegistravel>{
	@Autowired
	private AtividadeNaoRegistravelRepository  atividadeNaoRegistravelRepository;
	@Override
	public BaseRepository<AtividadeNaoRegistravel, Long> getRepository() {
		return this.atividadeNaoRegistravelRepository;
	}
}
