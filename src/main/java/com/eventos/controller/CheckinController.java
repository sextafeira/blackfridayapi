package com.eventos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.activities.Checkin;
import com.eventos.repository.CheckinRepository;
import com.eventos.repository.BaseRepository;

@RestController
@RequestMapping("/checkins")
public class CheckinController implements BaseController<Checkin>{
	@Autowired
	private CheckinRepository checkinRepository;
	
	@Override
	public BaseRepository<Checkin, Long> getRepository() {
		return this.checkinRepository;
	}
}
