package com.eventos.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.eventos.utils.Message;

@SpringBootApplication
@ComponentScan({"com.eventos.controller"})
@EnableJpaRepositories(basePackages = "com.eventos.repository")
@EntityScan(basePackages = {"com.eventos.model", "com.eventos.utils"})
@RestController
public class MainController{
    public static void main(String[] args) {
    	SpringApplication.run(MainController.class, args);
    }
    
    @RequestMapping(value = "/")
    public ResponseEntity<Message> index(@RequestParam(value="nome", defaultValue="empty") String name) {
    	return new ResponseEntity<Message>(Message.createSuccessMessage(String.format("%s", name)), HttpStatus.OK);
    }
}
