package com.eventos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.events.Instituicao;
import com.eventos.repository.BaseRepository;
import com.eventos.repository.InstituicaoRepository;

@RestController
@RequestMapping("/instituicoes")
public class InstituicaoController implements BaseController<Instituicao>{
	@Autowired
	private InstituicaoRepository instituicaoRepository;
	
	@Override
	public BaseRepository<Instituicao, Long> getRepository() {
		return this.instituicaoRepository;
	}
}
