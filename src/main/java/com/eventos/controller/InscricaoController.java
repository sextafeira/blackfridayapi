package com.eventos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.registration.Inscricao;
import com.eventos.repository.BaseRepository;
import com.eventos.repository.InscricaoRepository;

@RestController
@RequestMapping("/inscricoes")
public class InscricaoController implements BaseController<Inscricao>{
	
	@Autowired
	private InscricaoRepository registrationRepository;
	
	@Override
	public BaseRepository<Inscricao, Long> getRepository() {
		return this.registrationRepository;
	}
}
