package com.eventos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.events.Evento;
import com.eventos.repository.BaseRepository;
import com.eventos.repository.EventoRepository;

@RestController
@RequestMapping("/eventos")
public class EventoController implements BaseController<Evento>{
	@Autowired 
	private EventoRepository eventRepository;
	
	@Override
	public BaseRepository<Evento, Long> getRepository() {
		return this.eventRepository;
	}
}