package com.eventos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.local.LocalSimples;
import com.eventos.repository.BaseRepository;
import com.eventos.repository.LocalSimplesRepository;

@RestController
@RequestMapping("/locais-simples")
public class LocalSimplesController implements BaseController<LocalSimples>{
	@Autowired
	private LocalSimplesRepository localSimplesRepository;
	
	@Override
	public BaseRepository<LocalSimples, Long> getRepository() {
		return this.localSimplesRepository;
	}

}
