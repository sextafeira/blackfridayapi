package com.eventos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.eventos.model.activities.AtividadeRegistravelSimples;
import com.eventos.repository.AtividadeRegistravelSimplesRepository;
import com.eventos.repository.BaseRepository;

@RestController
@RequestMapping("/atividades-registraveis")
public class AtividadeRegistravelSimplesController implements BaseController<AtividadeRegistravelSimples>{
	@Autowired
	private AtividadeRegistravelSimplesRepository atividadeRegistravelSimplesRepository;

	@Override
	public BaseRepository<AtividadeRegistravelSimples, Long> getRepository() {
		return this.atividadeRegistravelSimplesRepository;
	}
}
