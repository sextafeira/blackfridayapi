package com.eventos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.events.Tag;
import com.eventos.repository.BaseRepository;
import com.eventos.repository.TagRepository;

@RestController
@RequestMapping("/tags")
public class TagController implements BaseController<Tag>{
	@Autowired 
	private TagRepository tagRepository;
	
	@Override
	public BaseRepository<Tag, Long> getRepository() {
		return this.tagRepository;
	}
}
