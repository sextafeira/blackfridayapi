package com.eventos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.registration.Pagamento;
import com.eventos.repository.BaseRepository;
import com.eventos.repository.PagamentoRepository;


@RestController
@RequestMapping("/pagamentos")
public class PagamentoController implements BaseController<Pagamento>{
	@Autowired
	private PagamentoRepository paymentRepository;

	@Override
	public BaseRepository<Pagamento, Long> getRepository() {
		return this.paymentRepository;
	}
}
