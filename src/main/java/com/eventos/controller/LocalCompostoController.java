package com.eventos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.local.LocalComposto;
import com.eventos.repository.BaseRepository;
import com.eventos.repository.LocalCompostoRepository;

@RestController
@RequestMapping("/locais-compostos")
public class LocalCompostoController implements BaseController<LocalComposto>{
	@Autowired
	private LocalCompostoRepository localCompostoRepository;
	
	@Override
	public BaseRepository<LocalComposto, Long> getRepository() {
		return this.localCompostoRepository;
	}
	
}
