package com.eventos.model.local;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, 
property = "id")
public class LocalSimples extends Local{
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	private Integer id;
	@Column(nullable=false)
	private Integer quantidadeDeVagasTotal;
	@Column(nullable=false)
	private Integer quantidadeDeVagasDisponiveis;
	@Column
	private Boolean ocupado;
	@Column 
	private TipoLocal tipoLocal;
	@ManyToOne
	@JoinColumn(name="local_pai_id", nullable=true)
	@JsonBackReference("local-filho")
	private LocalComposto localPai;
	
	protected LocalSimples() {
		super();
		this.ocupado = false;
		this.quantidadeDeVagasDisponiveis = 0;
		this.quantidadeDeVagasTotal = 0;
		this.tipoLocal = TipoLocal.SALA;
	}
	
	public LocalSimples(String nome, Integer quantidadeDeVagasTotal, TipoLocal tipoLocal) {
		this();
		super.setNome(nome);
		this.quantidadeDeVagasDisponiveis = this.quantidadeDeVagasTotal = quantidadeDeVagasTotal;
		this.tipoLocal = tipoLocal;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getQuantidadeDeVagasTotal() {
		return quantidadeDeVagasTotal;
	}
	public void setQuantidadeDeVagasTotal(Integer quantidadeDeVagasTotal) {
		this.quantidadeDeVagasTotal = quantidadeDeVagasTotal;
	}
	public Integer getQuantidadeDeVagasDisponiveis() {
		return quantidadeDeVagasDisponiveis;
	}
	public void setQuantidadeDeVagasDisponiveis(Integer quantidadeDeVagasDisponiveis) {
		this.quantidadeDeVagasDisponiveis = quantidadeDeVagasDisponiveis;
	}
	public Boolean getOcupado() {
		return ocupado;
	}
	
	public void setOcupado(Boolean ocupado) {
		this.ocupado = ocupado;
	}
	public TipoLocal getTipoLocal() {
		return tipoLocal;
	}
	public void setTipoLocal(TipoLocal tipoLocal) {
		this.tipoLocal = tipoLocal;
	}
	
	@Override
	public String toString() {
		return String.format(
				"LocalSimples [id=%s, quantidadeDeVagasTotal=%s, quantidadeDeVagasDisponiveis=%s, ocupado=%s, tipoLocal=%s]",
				id, quantidadeDeVagasTotal, quantidadeDeVagasDisponiveis, ocupado, tipoLocal);
	}
}
