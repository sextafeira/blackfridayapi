package com.eventos.model.local;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, 
property = "id")
public class LocalComposto extends Local{
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	private Integer id;
	@OneToMany(mappedBy="localPai")
	@JsonManagedReference("local-filho")
	private List<LocalSimples> locaisSimples;
	
	protected LocalComposto() {
		super();
		this.locaisSimples = new ArrayList<>();
	}
	
	protected LocalComposto(String nome) {
		super(nome);
		this.locaisSimples = new ArrayList<>();
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	public final List<LocalSimples> getLocaisSimples() {
		return Collections.unmodifiableList(locaisSimples);
	}
	
	@Deprecated
	public void setLocaisSimples(List<LocalSimples> locaisSimples) {
		for (LocalSimples localSimples : locaisSimples) {
			this.adicioneLocalSimples(localSimples);
		}
	}
	
	//Auxiliar:
	public void addLocalSimples(LocalSimples localSimples) {
		this.locaisSimples.add(localSimples);
	}
	
	public void adicioneLocalSimples(LocalSimples localSimples) {
		this.locaisSimples.add(localSimples);
	}
	
	@Override
	public String toString() {
		return String.format("LocalComposto [id=%s, locaisSimples=%s]", id, locaisSimples);
	}
}