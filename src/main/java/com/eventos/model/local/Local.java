package com.eventos.model.local;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import com.eventos.utils.ValidadorGenerico;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, 
property = "id")
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class Local{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable=false)
	private String nome;
	
	protected Local(){
		nome = "Espaco Padrao";
	}
	
	public Local(String nome) {
		this();
		this.nome = nome;
	}
	
	public void setName(String nome){
		ValidadorGenerico.checkNull(nome);
		this.nome = nome;
	}
	
	//?factory?
	public static LocalSimples getLocalPadrao() {
		return new LocalSimples();
	}
	
	public static LocalComposto getLocalCompostoPadrao() {
		return new LocalComposto();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return String.format("Local [id=%s, nome=%s]", id, nome);
	}
}
