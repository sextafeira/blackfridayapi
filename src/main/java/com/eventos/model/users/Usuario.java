package com.eventos.model.users;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;

import com.eventos.model.activities.Checkin;
import com.eventos.model.events.Evento;
import com.eventos.model.events.Tag;
import com.eventos.model.registration.Inscricao;
import com.eventos.utils.ValidadorGenerico;
import com.eventos.utils.Hash;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Usuario.class)
public class Usuario {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @Column(length=100)
    @NotNull(message="Nome nao pode ser nulo")
    private String nomeCompleto;
    @Column
    @NotNull(message="E-mail nao pode ser nulo")
    @Email(message="E-mail precisa ser valido")
    private String email;
    @Column(nullable=false,length=128)
    @NotNull(message="Senha nao pode ser nula")
    private String senha;
    @Column(nullable=false)
    private String codigoDeConfirmacao;
	@Column(nullable=false)
	private StatusUsuario statusUsuario;
	
	/*
	 * OBS: Usuario esta cuidado de coisas que nao deveria
	 * cuidar, mudar situacao de usuario para atender o principio
	 * do SRP
	 * */
	
	@ManyToMany(cascade= {CascadeType.ALL}, fetch=FetchType.LAZY)
	@JoinTable(name = "usuario_tag", joinColumns =  @JoinColumn(name = "usuario_id"),
        inverseJoinColumns = @JoinColumn(name = "tag_id"))
	private List<Tag> tags;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="usuarioInscrito")
	@JsonManagedReference("inscricao-user")
	private List<Inscricao> inscricoes;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="usuarioCriador")
	@JsonManagedReference("evento-usuario")
	private List<Evento> eventosCriados;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="administradores")
	private List<Evento> eventosAdministrados;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="usuarioQueFoiChecado")
	@JsonManagedReference("checkin-usuario")
	private List<Checkin> checkins;
	
	private Usuario() {
		this.eventosCriados = new ArrayList<>();
		this.eventosAdministrados = new ArrayList<>();
		this.inscricoes = new ArrayList<>();
		this.tags = new ArrayList<>();
		this.checkins = new ArrayList<>();
		
		this.statusUsuario = StatusUsuario.PENDENTE;
		this.codigoDeConfirmacao = Hash.getStringHash(20);
	}

	public Usuario(String email, String password) {
		this();
		this.setEmail(email);;
		this.setSenha(password);
	}
	
	//?Factory? + NullObject
	public static Usuario getUsuarioPadrao() {
		return new Usuario("user@user.com", "Usuario padrao");
	}
	
	//Getters, Setters:
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public final List<Checkin> getCheckins() {
		return Collections.unmodifiableList(checkins);
	}
	
	@Deprecated
	public void setCheckins(List<Checkin> checkins) {
		for (Checkin checkin : checkins) {
			this.adicioneCheckin(checkin);
		}
	}
	

	public String getNomeCompleto() {
		return nomeCompleto;
	}

	public void setNomeCompleto(String nomeCompleto) {
		this.nomeCompleto = nomeCompleto;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		ValidadorGenerico.checkEmail(email);
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = Hash.getSHA1(senha);;
	}

	public String getCodigoDeConfirmacao() {
		return codigoDeConfirmacao;
	}

	public void setCodigoDeConfirmacao(String codigoDeConfirmacao) {
		this.codigoDeConfirmacao = codigoDeConfirmacao;
	}

	public StatusUsuario getStatusUsuario() {
		return statusUsuario;
	}

	public void setStatusUsuario(StatusUsuario statusUsuario) {
		this.statusUsuario = statusUsuario;
	}

	public final List<Tag> getTags() {
		return Collections.unmodifiableList(tags);
	}
	
	@Deprecated
	public void setTags(List<Tag> tags) {
		for (Tag tag : tags) {
			this.adicioneUmaTag(tag);
		}
	}

	public final List<Inscricao> getInscricoes() {
		return Collections.unmodifiableList(inscricoes);
	}
	
	@Deprecated
	public void setInscricoes(List<Inscricao> inscricoes) {
		for (Inscricao inscricao : inscricoes) {
			this.adicioneUmaInscricao(inscricao);
		}
	}
	
	public final List<Evento> getEventosCriados() {
		return Collections.unmodifiableList(eventosCriados);
	}
	
	@Deprecated
	public void setEventosCriados(List<Evento> eventosCriados) {
		for (Evento evento : eventosCriados) {
			this.adicionarEventoCriado(evento);
		}
	}

	public final List<Evento> getEventosAdministrados() {
		return Collections.unmodifiableList(eventosAdministrados);
	}
	
	@Deprecated
	public void setEventosAdministrados(List<Evento> eventosAdministrados) {
		for (Evento evento : eventosAdministrados) {
			this.adicioneUmEventoAdministrado(evento);
		}
	}	


	@Override
	public String toString() {
		return String.format(
				"Usuario [id=%s, nomeCompleto=%s, email=%s, senha=%s, codigoDeConfirmacao=%s, statusUsuario=%s, tags=%s, inscricoes=%s, eventosCriados=%s, eventosAdministrados=%s, checkins=%s]",
				id, nomeCompleto, email, senha, codigoDeConfirmacao, statusUsuario, tags, inscricoes, eventosCriados,
				eventosAdministrados, checkins);
	}
	
	//Auxiliares:
	public void adicioneCheckin(Checkin checkin) {
		this.checkins.add(checkin);
	}
	
	public void adicioneUmEventoAdministrado(Evento eventoAdministrado) {
		this.eventosAdministrados.add(eventoAdministrado);
	}
	
	public boolean isValidConfirmationCode(String codigoDeConfirmacao) {
		return this.codigoDeConfirmacao.equals(codigoDeConfirmacao);
	}
	
	public void adicioneUmaInscricao(Inscricao inscricao) {
		this.inscricoes.add(inscricao);
	}
	
	public void adicionarEventoCriado(Evento eventoCriado) {
		this.eventosCriados.add(eventoCriado);
	}
	
	public void adicioneUmaTag(Tag tag) {
		this.tags.add(tag);
	}
}