package com.eventos.model.registration;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.eventos.utils.ValidadorGenerico;
import com.eventos.utils.Hash;
import com.eventos.utils.LocalDateTimeAttributeConverter;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, 
property = "id")
public class Cupom{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable=false)
	@JsonProperty(access = Access.READ_ONLY)
	private String code;
	
	@Column(nullable=false)
	private Double porcentagemDesconto;
	
	@Column(nullable=false)
	@JsonFormat(pattern = ValidadorGenerico.PATTERN_DATA_HORA)
	@Convert(converter=LocalDateTimeAttributeConverter.class)
	private LocalDateTime dataInicio;
	
	@Column(nullable=true)
	@JsonFormat(pattern = ValidadorGenerico.PATTERN_DATA_HORA)
	@Convert(converter=LocalDateTimeAttributeConverter.class)
	private LocalDateTime dataTermino;
	
	@Column(nullable=false)
	private TipoCupom tipoCupom;
	
	private Cupom() {
		this.code = Hash.getStringHash(20);
	}
	
	private Cupom(Double discountPercent, TipoCupom couponType) {
		this();
		this.code = Hash.getStringHash(20);
		this.porcentagemDesconto = discountPercent;
		this.dataInicio = LocalDateTime.now();
		this.dataTermino = LocalDateTime.MAX;
	}
	
	private Cupom(Double discountPercent, TipoCupom tipoCupom, LocalDateTime dataInicio, LocalDateTime dataTermino) {
		this(discountPercent, tipoCupom);
		this.dataInicio = dataInicio;
		this.dataTermino = dataTermino;
	}
	
	public static Cupom getAutomaticCoupon(Double discountPercent, LocalDateTime dataInicio, LocalDateTime dataTermino){
		return new Cupom(discountPercent, TipoCupom.AUTOMATICO, dataInicio, dataTermino);
	}
	
	public static Cupom getNormalCoupon(Double discountPercent, LocalDateTime dataInicio, LocalDateTime dataTermino){
		return new Cupom(discountPercent, TipoCupom.NORMAL, dataInicio, dataTermino);
	}
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}
	
	@Deprecated
	private void setCode(String code) {
		this.code = code;
	}

	public Double getPorcentagemDesconto() {
		return porcentagemDesconto;
	}

	public void setPorcentagemDesconto(Double porcentagemDesconto) {
		this.porcentagemDesconto = porcentagemDesconto;
	}

	public LocalDateTime getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(LocalDateTime dataInicio) {
		this.dataInicio = dataInicio;
	}

	public LocalDateTime getDataTermino() {
		return dataTermino;
	}

	public void setDataTermino(LocalDateTime dataTermino) {
		this.dataTermino = dataTermino;
	}

	public TipoCupom getTipoCupom() {
		return tipoCupom;
	}

	public void setTipoCupom(TipoCupom tipoCupom) {
		this.tipoCupom = tipoCupom;
	}

	@Override
	public String toString() {
		return String.format(
				"Cupom [id=%s, code=%s, porcentagemDesconto=%s, dataInicio=%s, dataTermino=%s, tipoCupom=%s]",
				id, code, porcentagemDesconto, dataInicio, dataTermino, tipoCupom);
	}	
}
