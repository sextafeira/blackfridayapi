package com.eventos.model.registration;

public enum StatusInscricao {
	NOVA, APROVADA, NEGADA;
}
