package com.eventos.model.registration;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import com.eventos.model.activities.AtividadeRegistravelSimples;
import com.eventos.model.activities.PacoteDeAtividades;
import com.eventos.model.events.EstadoEvento;
import com.eventos.model.events.Evento;
import com.eventos.model.users.Usuario;
import com.eventos.utils.Hash;
import com.eventos.utils.ValidadorGenerico;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, 
property = "id")
public class Inscricao{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column
	private String codigoDeInscricao;

	@ManyToOne
    @JoinColumn(name="usuario_id", nullable=false)
	@JsonBackReference("inscricao-user")
	private Usuario usuarioInscrito;
	
	@Column
	private Double valorTotal;
	
	@Column(nullable=false)
	private StatusInscricao statusInscricao;

	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="evento_id", nullable=false)
	@JsonBackReference("inscricao-evento")
	private Evento evento;
	
	@Column
	private LocalDate dataInscricao;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="cupom_aplicado_id", nullable=false)
	@JsonBackReference("inscricao-cupom-aplicado")
	private Cupom cupomNormal;
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="cupom_temporario_id", nullable=false)
	@JsonBackReference("inscricao-cupom-temporario")
	private Cupom cupomTemporario;
	@OneToOne
	@PrimaryKeyJoinColumn
	private Pagamento pagamento;
	@OneToMany
    @JoinColumn(name="inscricao_id", nullable=false)
	@JsonBackReference
	private List<AtividadeRegistravelSimples> atividades;
	 

	private Inscricao() {
		this.valorTotal = 0.0;
		this.pagamento = new Pagamento(this);
		this.dataInscricao = LocalDate.now();
		this.statusInscricao = StatusInscricao.NOVA;
		this.codigoDeInscricao = Hash.getStringHash(20);
		this.atividades = new ArrayList<>();
		this.cupomNormal = Cupom.getNormalCoupon(0d, LocalDateTime.now(), LocalDateTime.MAX);
		this.cupomTemporario = Cupom.getAutomaticCoupon(0d, LocalDateTime.now(), LocalDateTime.MAX);
	}
	
	public Inscricao(Evento evento) {
		this();
		this.setEvento(evento);
		this.evento = evento;
		this.atividades = this.evento.getAtividades();	
	}
	
	public Inscricao(PacoteDeAtividades pacote) {
		this();
		ValidadorGenerico.checkNull(pacote);
		this.evento = pacote.getEvento();
		this.atividades = pacote.getAtividadeRegistravelSimples();
	}
	
	public Inscricao(List<AtividadeRegistravelSimples>atividades){
		this();
		ValidadorGenerico.checkNull(atividades);
		for (AtividadeRegistravelSimples atividadeRegistravelSimples : atividades) {
			this.adicioneUmaAtividade(atividadeRegistravelSimples);
		}
	}
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigoDeInscricao() {
		return codigoDeInscricao;
	}

	public void setCodigoDeInscricao(String codigoDeInscricao) {
		this.codigoDeInscricao = codigoDeInscricao;
	}

	public Usuario getUsuarioInscrito() {
		return usuarioInscrito;
	}

	public void setUsuarioInscrito(Usuario usuarioInscrito) {
		this.usuarioInscrito = usuarioInscrito;
	}

	public StatusInscricao getStatusInscricao() {
		return statusInscricao;
	}

	public void setStatusInscricao(StatusInscricao statusInscricao) {
		this.statusInscricao = statusInscricao;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		if(evento == null)
			throw new RuntimeException("Evento nao pode ser Nulo");
		if(!evento.getEstadoEvento().equals(EstadoEvento.ABERTO))
			throw new IllegalArgumentException("Evento nao esta aberto para inscricoes!");
		this.evento = evento;
	}

	public Cupom getCupomNormal() {
		return cupomNormal;
	}

	public void setCupomNormal(Cupom cupomAplicado) {
		this.cupomNormal = cupomAplicado;
	}

	public Cupom getCupomTemporario() {
		return cupomTemporario;
	}

	public void setCupomTemporario(Cupom cupomTemporario){
		if(getCupomTemporario() != cupomTemporario)
			throw new IllegalArgumentException("Nao podem haver dois Cupons Automaticos ativos ao mesmo tempo");
		this.cupomTemporario = cupomTemporario;
	}

	public Pagamento getPagamento() {
		return pagamento;
	}

	public void setPagamento(Pagamento pagamento) {
		this.pagamento = pagamento;
	}

	public final List<AtividadeRegistravelSimples> getAtividades() {
		return Collections.unmodifiableList(atividades);
	}
	@Deprecated
	public void setAtividades(List<AtividadeRegistravelSimples> atividades) {
		for (AtividadeRegistravelSimples atividadeRegistravelSimples : atividades) {
			this.adicioneUmaAtividade(atividadeRegistravelSimples);
		}
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Double getValorTotal() {
		
		if (this.atividades.size() == 0) return this.valorTotal;
		
		for (AtividadeRegistravelSimples item : this.atividades){
			this.valorTotal += item.getPrice();
		}
		
		if(cupomNormal != null)	
			this.valorTotal -= this.valorTotal * (cupomNormal.getPorcentagemDesconto() / 100.0d);
		if(cupomTemporario != null)	
			this.valorTotal -= this.valorTotal * (cupomTemporario.getPorcentagemDesconto() / 100.0d); 
				
		return this.valorTotal;
	}
	
	//Auxiliar:
	public void pagar(Usuario usuarioCriador) {
		this.pagamento.setUsuarioRegistrador(usuarioCriador);
		this.pagamento.setPago(true);
	}
	
	public void adicioneUmaAtividade(AtividadeRegistravelSimples atividade){
		if(this.evento == null) {
			this.evento = atividade.getEvento();
		}
		
		if(this.pagamento.getPago()) {
			throw new IllegalArgumentException("Essa atividade já está paga");
		}
	
		for(Evento secondary : this.evento.getEventosSatelite()) {
			if(secondary.equals(atividade.getEvento())) {
				this.atividades.add(atividade);
				return;
			}
		}
		
		if (!this.getEvento().equals(atividade.getEvento())) 
			throw new IllegalArgumentException("This activity cannot be combined to the registration");
		
		for (AtividadeRegistravelSimples nonActivity : atividade.getAtividadesQueNaoPodemFicarParalelas()) {
			if(atividade.equals(nonActivity)) {
				throw new IllegalArgumentException("This activity cannot be aggregated");
			}
		}
		
		this.atividades.add(atividade);
	}

	@Override
	public String toString() {
		return String.format(
				"Inscricao [id=%s, codigoDeInscricao=%s, usuarioInscrito=%s, valorTotal=%s, statusInscricao=%s, evento=%s, dataInscricao=%s, totalValue=%s, cupomAplicado=%s, cupomTemporario=%s, pagamento=%s, atividades=%s]",
				id, codigoDeInscricao, usuarioInscrito, valorTotal, statusInscricao, evento, dataInscricao, valorTotal,
				cupomNormal, cupomTemporario, pagamento, atividades);
	}
}