package com.eventos.model.registration;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.eventos.model.users.Usuario;
import com.eventos.utils.LocalDateTimeAttributeConverter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, 
property = "id")
public class Pagamento{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable=false)
	@Convert(converter=LocalDateTimeAttributeConverter.class)
	private LocalDateTime dataPagamento;
	
	@Column(nullable=false)
	private Boolean pago;
	
	@OneToOne(cascade=CascadeType.ALL, mappedBy="pagamento")
	private Inscricao inscricao;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="usuario_registrador_id", nullable=false)
	private Usuario usuarioRegistrador;
	
	private Pagamento() {
		this.dataPagamento = LocalDateTime.now();
		this.pago = false;
	}
	
	public Pagamento(Inscricao inscricao) {
		this();
		this.inscricao = inscricao;
	}
	
	public Pagamento(Inscricao registration, Usuario creator) {
		this(registration);
		this.usuarioRegistrador = creator;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(LocalDateTime dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public Boolean getPago() {
		return this.pago;
	}

	public void setPago(Boolean pago) {
		this.pago = pago;
	}

	public Inscricao getInscricao() {
		return inscricao;
	}

	public void setInscricao(Inscricao inscricao) {
		this.inscricao = inscricao;
	}

	public Usuario getUsuarioRegistrador() {
		return usuarioRegistrador;
	}

	public void setUsuarioRegistrador(Usuario usuarioRegistrador) {
		this.usuarioRegistrador = usuarioRegistrador;
	}

	@Override
	public String toString() {
		return String.format(
				"Pagamento [id=%s, dataPagamento=%s, pago=%s, inscricao=%s, usuarioRegistrador=%s]", id,
				dataPagamento, pago, inscricao, usuarioRegistrador);
	}
}
