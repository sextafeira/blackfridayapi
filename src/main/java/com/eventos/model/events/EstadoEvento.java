package com.eventos.model.events;

public enum EstadoEvento {
	NOVO, ABERTO, TERMINADO, CANCELADO;
}
