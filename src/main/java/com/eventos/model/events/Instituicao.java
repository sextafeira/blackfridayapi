package com.eventos.model.events;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.eventos.utils.ValidadorGenerico;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Instituicao {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column(nullable=false)
	private String nome;
	@Column
	private String descricao;
	@Column
	private String telefone;
	@Column
	private String website;
	@Column
	private String foto;
	
	@ManyToOne
	@JoinColumn(name="evento_id", nullable=true)
	@JsonBackReference("evento-instituicao")
	private Evento evento;
	
	@Column(nullable=false)
	private TipoRelacionamento tipoRelacionamento;
	
	private Instituicao() {
		
	}
	
	public Instituicao(Evento evento, String nome) {
		this.evento = evento;
		this.nome = nome;
	}
	
	//Getters - Setters
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		ValidadorGenerico.checkUrl(website);
		this.website = website;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public Evento getEvento() {
		return evento;
	}
	public void setEvento(Evento evento) {
		this.evento = evento;
	}
	public TipoRelacionamento getTipoRelacionamento() {
		return tipoRelacionamento;
	}
	public void setTipoRelacionamento(TipoRelacionamento tipoRelacionamento) {
		this.tipoRelacionamento = tipoRelacionamento;
	}
	
	@Override
	public String toString() {
		return String.format(
				"Instituicao [id=%s, nome=%s, descricao=%s, telefone=%s, website=%s, foto=%s, evento=%s, tipoRelacionamento=%s]",
				id, nome, descricao, telefone, website, foto, evento, tipoRelacionamento);
	}
}
