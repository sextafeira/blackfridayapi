package com.eventos.model.events;

public enum TipoRelacionamento {
	PARCERIA, PATROCINIO, COLABORACAO, IDEALIZACAO;
}
