package com.eventos.model.events;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.eventos.model.activities.AtividadeRegistravelSimples;
import com.eventos.model.activities.Duravel;
import com.eventos.model.activities.PacoteDeAtividades;
import com.eventos.model.activities.Trilha;
import com.eventos.model.local.Local;
import com.eventos.model.local.LocalComposto;
import com.eventos.model.registration.Inscricao;
import com.eventos.model.users.Usuario;
import com.eventos.utils.ValidadorGenerico;
import com.eventos.utils.LocalDateAttributeConverter;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Evento implements Duravel{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column(nullable=false)
	private String nome;
	@Column
	private String descricao;
	@Column(nullable=true)
	private String foto;
	@Column
	private Double preco;
	
	@Column
	@Convert(converter=LocalDateAttributeConverter.class)
	@JsonProperty(access = Access.READ_ONLY)
	@JsonFormat(pattern = ValidadorGenerico.PATTERN_DATA)
	private LocalDate dataCriacao;
	
	@ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="usuario_id", nullable=true)
	@JsonBackReference("evento-usuario")
	private Usuario usuarioCriador;
	@Column
	@Convert(converter=LocalDateAttributeConverter.class)
	@JsonFormat(pattern = ValidadorGenerico.PATTERN_DATA)
	private LocalDate dataInicio;
	@Column
	@Convert(converter=LocalDateAttributeConverter.class)
	@JsonFormat(pattern = ValidadorGenerico.PATTERN_DATA)
	private LocalDate dataTermino;
	@Column
	private EstadoEvento estadoEvento;
	@Column
	private TipoEvento tipoEvento;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name = "evento_tag", joinColumns =  @JoinColumn(name = "evento_id"),
        inverseJoinColumns = @JoinColumn(name = "tag_id"))
	private List<Tag> tags;
	
	@OneToMany(mappedBy="evento")
	@JsonManagedReference(value="evento-instituicao")
	private List<Instituicao> instituicoes;
	
	@OneToMany(mappedBy="evento")
	@JsonManagedReference(value="inscricao-evento")
	private List<Inscricao> inscricoes;
	
	@OneToMany(mappedBy="eventoPai")
	@JsonManagedReference(value="evento-evento-satelite")
	private List<Evento> eventosSatelite;
	
	@OneToMany(mappedBy="evento")
	@JsonManagedReference(value="atividade-evento")
	private List<AtividadeRegistravelSimples> atividades;
	
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER, optional=true)
	private LocalComposto local;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name = "administradores_evento", joinColumns =  @JoinColumn(name = "evento_id"),
        inverseJoinColumns = @JoinColumn(name = "usuario_id"))
	private List<Usuario> administradores;
	
	@JsonProperty(access = Access.WRITE_ONLY)
	@ManyToOne(cascade= CascadeType.ALL)
	@JoinColumn(name="evento_id")
	@JsonBackReference("evento-evento-satelite")
	private Evento eventoPai;
	

	@OneToMany(mappedBy="evento")
	@JsonManagedReference(value="evento-trilha")
	private List<Trilha> trilhas;	
	
	
	@OneToMany(mappedBy="evento")
	@JsonManagedReference(value="evento-pacote")
	private List<PacoteDeAtividades> pacotes;
	
	
	private Evento() {
		this.atividades = new ArrayList<>();
		this.tags = new ArrayList<>();
		this.instituicoes = new ArrayList<>();
		this.eventosSatelite = new ArrayList<>();
		this.inscricoes = new ArrayList<>();
		this.administradores = new ArrayList<>();
		this.pacotes = new ArrayList<>();
		this.trilhas = new ArrayList<>();		
		this.tipoEvento = TipoEvento.MASTER;
		this.eventoPai = this;
		this.dataCriacao = LocalDate.now();
		this.estadoEvento = EstadoEvento.NOVO;
		this.dataInicio = LocalDate.now();
		this.dataTermino = LocalDate.MAX;
		this.local = Local.getLocalCompostoPadrao();
	}
	
	public Evento(String nome, Usuario usuarioCriador) {
		this();
		this.setNome(nome);
		this.setUsuarioCriador(usuarioCriador);
	}
	

	//Getters - Setters:
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		ValidadorGenerico.checkEmptyString(nome);
		this.nome = nome;
	}
		
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public Double getPreco() {
		return preco;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}

	
	public LocalDate getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(LocalDate dataCriacao) {
		this.dataCriacao = dataCriacao;
	}
	
	public Usuario getUsuarioCriador() {
		return usuarioCriador;
	}

	public void setUsuarioCriador(Usuario usuarioCriador) {
		ValidadorGenerico.checkNull(usuarioCriador);
		this.usuarioCriador = usuarioCriador;
	}
	
	public LocalDateTime getDataInicio() {
		return dataInicio.atStartOfDay();
	}

	public void setDataInicio(LocalDate dataInicio) {
		this.dataInicio = dataInicio;
	}

	public LocalDateTime getDataTermino() {
		return dataTermino.atStartOfDay();
	}
	
	public void setDataTermino(LocalDate dataTermino) {
		this.dataTermino = dataTermino;
	}
	
	public EstadoEvento getEstadoEvento() {
		return estadoEvento;
	}

	public void setEstadoEvento(EstadoEvento estadoEvento) {
		this.estadoEvento = estadoEvento;
	}

	public TipoEvento getTipoEvento() {
		return tipoEvento;
	}

	public void setTipoEvento(TipoEvento tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	public final List<Tag> getTags() {
		return Collections.unmodifiableList(tags);
	}

	public final List<Instituicao> getInstituicoes() {
		return Collections.unmodifiableList(instituicoes);
	}
	
	@Deprecated
	public void setInstituicoes(List<Instituicao> instituicoes) {
		for (Instituicao instituicao : instituicoes) {
			this.adicionarInstituicao(instituicao);
		}
	}
	
	public final List<Evento> getEventosSatelite() {
		return Collections.unmodifiableList(eventosSatelite);
	}
	@Deprecated
	public void setEventosSatelite(List<Evento> eventosSatelite) {
		for (Evento evento : eventosSatelite) {
			this.adicioneEventoSatelite(evento);
		}
	}

	public final List<AtividadeRegistravelSimples> getAtividades() {
		return Collections.unmodifiableList(atividades);
	}
	@Deprecated
	public void setAtividades(List<AtividadeRegistravelSimples> atividades) {
		for (AtividadeRegistravelSimples atividadeRegistravelSimples : atividades) {
			this.addAtividade(atividadeRegistravelSimples);
		}
	}

	public LocalComposto getLocal() {
		return local;
	}

	public void setLocal(LocalComposto local) {
		this.local = local;
	}

	public final List<Usuario> getAdministradores() {
		return Collections.unmodifiableList(administradores);
	}
	@Deprecated
	public void setAdministradores(List<Usuario> administradores) {
		for (Usuario usuario : administradores) {
			this.adicioneAdministrador(usuario);
		}
	}


	public Evento getEventoPai() {
		return eventoPai;
	}

	public void setEventoPai(Evento eventoPai) {
		this.eventoPai = eventoPai;
	}

	public final List<Inscricao> getInscricoes() {
		return Collections.unmodifiableList(inscricoes);
	}
	
	@Deprecated
	public void setInscricoes(List<Inscricao> inscricoes) {
		for (Inscricao inscricao : inscricoes) {
			this.adicioneUmaInscricao(inscricao);
		}
	}
	

	public final List<PacoteDeAtividades> getPacotes() {
		return Collections.unmodifiableList(pacotes);
	}
	
	@Deprecated
	public void setPacotes(List<PacoteDeAtividades> pacotes) {
		for (PacoteDeAtividades pacoteDeAtividade : pacotes) {
			this.adicioneUmPacote(pacoteDeAtividade);
		}
	}
	
	public void adicioneUmaInscricao(Inscricao inscricao) {
		this.inscricoes.add(inscricao);
	}
	
	@Deprecated
	public void addSecondary(Evento event) {
		this.adicioneEventoSatelite(event);
	}
	//
	public void adicionarInstituicao(Instituicao instituicao) {
		this.instituicoes.add(instituicao);
	}
	
	public void adicioneUmaTag(String nomeDaTag) {
		Tag novaTag = new Tag(nomeDaTag);
		novaTag.adicioneUmEvento(this);
		this.adicioneUmaTag(novaTag);
	}
	public void adicioneUmaTag(Tag tag) {
		this.tags.add(tag);
	}
	
	public void adicioneEventoSatelite(Evento eventoSatelite) {
		if(!eventoSatelite.equals(this)) {
			if(!eventoSatelite.getTipoEvento().equals(TipoEvento.SATELITE)) {
				eventoSatelite.setTipoEvento(TipoEvento.SATELITE);
				this.eventosSatelite.add(eventoSatelite);
				eventoSatelite.setEventoPai(this);
			}
		}

	}
	public void addAtividade(String name) {
		AtividadeRegistravelSimples atividade = new AtividadeRegistravelSimples(name);
		atividade.setEvento(this);	
		this.atividades.add(atividade);
	}

	public void addAtividade(AtividadeRegistravelSimples atividadeRegistravel) {
		atividadeRegistravel.setEvento(this);
		this.atividades.add(atividadeRegistravel);
	}
	
	public void adicioneAdministrador(Usuario usuario) {
		this.administradores.add(usuario);
	}
	
	public void adicioneUmPacote(PacoteDeAtividades pacote) {
		this.pacotes.add(pacote);
	}
	
	@Override
	public String toString() {
		return String.format(
				"Evento [id=%s, nome=%s, descricao=%s, foto=%s, preco=%s, dataCriacao=%s, usuarioCriador=%s, dataInicio=%s, dataTermino=%s, estadoEvento=%s, tipoEvento=%s, tags=%s, instituicoes=%s, inscricoes=%s, eventosSatelite=%s, atividades=%s, local=%s, administradores=%s, eventoPai=%s]",
				id, nome, descricao, foto, preco, dataCriacao, usuarioCriador, dataInicio, dataTermino, estadoEvento,
				tipoEvento, tags, instituicoes, inscricoes, eventosSatelite, atividades, local, administradores,
				eventoPai);
	}
}