package com.eventos.model.events;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.eventos.model.users.Usuario;
import com.eventos.utils.ValidadorGenerico;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Usuario.class)
public class Tag {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable=false,unique=true)
	private String nome;
	
	@JsonProperty(access = Access.WRITE_ONLY)
	@ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="tags")
	private List<Usuario> usuarios;
	
	@JsonProperty(access = Access.WRITE_ONLY)
	@ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="tags")
	private List<Evento> eventos;
	
	private Tag() {
		this.eventos = new ArrayList<>();
		this.usuarios = new ArrayList<>();
	}
	
	public Tag(String nome) {
		this();
		this.setNome(nome);
	}
	//Getters-Setters:
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		ValidadorGenerico.checkEmptyString(nome);
		this.nome = nome;
	}
	
	public final List<Usuario> getUsuarios() {
		return Collections.unmodifiableList(usuarios);
	}
	
	@Deprecated
	public void setUsuarios(List<Usuario> usuarios) {
		for (Usuario usuario : usuarios) {
			this.adicioneUmUsuario(usuario);
		}
	}
	
	public final List<Evento> getEventos() {
		return Collections.unmodifiableList(eventos);
	}
	
	//Auxiliar:
	public void adicioneUmUsuario(Usuario usuario) {
		this.usuarios.add(usuario);
	}
	
	public void adicioneUmEvento(Evento evento) {
		evento.adicioneUmaTag(this);
		this.eventos.add(evento);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag other = (Tag) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("Tag [id=%s, nome=%s, usuarios=%s, eventos=%s]", id, nome, usuarios, eventos);
	}
}
