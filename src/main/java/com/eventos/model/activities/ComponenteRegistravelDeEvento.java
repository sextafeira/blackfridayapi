package com.eventos.model.activities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.eventos.model.users.Usuario;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, 
property = "id")
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class ComponenteRegistravelDeEvento{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	protected Long id;
	@Column(nullable=false)
	protected String nome;
	@Column
	protected String descricao;
	
	@ManyToOne
	@JoinColumn(name="usuario_id", nullable=true)
	@JsonBackReference("atividade-usuario")
	protected Usuario usuarioCriador;
	
	private ComponenteRegistravelDeEvento(){
		
	}
	
	protected ComponenteRegistravelDeEvento(String nome) {
		this();
		this.nome = nome;	
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Usuario getUsuarioCriador() {
		return usuarioCriador;
	}

	public void setUsuarioCriador(Usuario usuarioCriador) {
		this.usuarioCriador = usuarioCriador;
	}

	@Override
	public String toString() {
		return String.format(
				"AtividadeRegistravel [id=%s, nome=%s, descricao=%s]", id, nome,
				descricao);
	}
}
