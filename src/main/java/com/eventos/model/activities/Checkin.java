package com.eventos.model.activities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.eventos.model.users.Usuario;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, 
property = "id")
public class Checkin {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column
	private LocalDateTime momentoEmQueFoiFeitaAChecagem;
	@Column
	private String descricao;
	@ManyToOne
	@JsonBackReference("checkin-usuario")
	private Usuario usuarioQueFoiChecado;
	@ManyToOne
	@JsonBackReference("atividade-checkin")
	private AtividadeRegistravelSimples atividade;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public LocalDateTime getMomentoEmQueFoiFeitaAChecagem() {
		return momentoEmQueFoiFeitaAChecagem;
	}
	public void setMomentoEmQueFoiFeitaAChecagem(LocalDateTime momentoEmQueFoiFeitaAChecagem) {
		this.momentoEmQueFoiFeitaAChecagem = momentoEmQueFoiFeitaAChecagem;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Usuario getUsuarioQueFoiChecado() {
		return usuarioQueFoiChecado;
	}
	public void setUsuarioQueFoiChecado(Usuario usuarioQueFoiChecado) {
		this.usuarioQueFoiChecado = usuarioQueFoiChecado;
	}
	public AtividadeRegistravelSimples getAtividadeRegistravelSimples() {
		return atividade;
	}
	public void setAtividadeRegistravelSimples(AtividadeRegistravelSimples atividadeRegistravelSimples) {
		this.atividade = atividadeRegistravelSimples;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((atividade == null) ? 0 : atividade.hashCode());
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((momentoEmQueFoiFeitaAChecagem == null) ? 0 : momentoEmQueFoiFeitaAChecagem.hashCode());
		result = prime * result + ((usuarioQueFoiChecado == null) ? 0 : usuarioQueFoiChecado.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Checkin other = (Checkin) obj;
		if (atividade == null) {
			if (other.atividade != null)
				return false;
		} else if (!atividade.equals(other.atividade))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (momentoEmQueFoiFeitaAChecagem == null) {
			if (other.momentoEmQueFoiFeitaAChecagem != null)
				return false;
		} else if (!momentoEmQueFoiFeitaAChecagem.equals(other.momentoEmQueFoiFeitaAChecagem))
			return false;
		if (usuarioQueFoiChecado == null) {
			if (other.usuarioQueFoiChecado != null)
				return false;
		} else if (!usuarioQueFoiChecado.equals(other.usuarioQueFoiChecado))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return String.format(
				"Checkin [id=%s, momentoEmQueFoiFeitaAChecagem=%s, descricao=%s, usuarioQueFoiChecado=%s, atividadeRegistravelSimples=%s]",
				id, momentoEmQueFoiFeitaAChecagem, descricao, usuarioQueFoiChecado, atividade);
	}
}
