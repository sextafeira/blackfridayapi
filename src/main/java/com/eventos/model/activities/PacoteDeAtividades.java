package com.eventos.model.activities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.eventos.model.events.Evento;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, 
property = "id")
public final class PacoteDeAtividades extends ComponenteRegistravelDeEvento{
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	private Long id;
	@Column
	private Double preco;
	
	@ManyToOne
	@JoinColumn(name="evento_id", nullable=true)
	@JsonBackReference("evento-pacote")
	protected Evento evento;
	
	@ManyToMany
	@JoinTable(name = "pacote_pacote_excluido", joinColumns =  @JoinColumn(name = "pacote_id"),
    inverseJoinColumns = @JoinColumn(name = "pacote_excluido_id"))
	private List<PacoteDeAtividades> pacotesQueNaoPodemSerCombinados;
	
	@OneToMany(mappedBy="pacoteDeAtividades")
	@JsonManagedReference("atividade-pacote")
	private List<AtividadeRegistravelSimples> atividadeRegistravelSimples;
	
	public PacoteDeAtividades(String nome) {
		super(nome);
		this.atividadeRegistravelSimples = new ArrayList<>();
		this.pacotesQueNaoPodemSerCombinados = new ArrayList<>();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getPreco() {
		return preco;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}
	
	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public final List<AtividadeRegistravelSimples> getAtividadeRegistravelSimples() {
		return Collections.unmodifiableList(atividadeRegistravelSimples);
	}
	
	@Deprecated
	public void setAtividadeRegistravelSimples(List<AtividadeRegistravelSimples> atividadesRegistravelSimples) {
		for (AtividadeRegistravelSimples atividadeRegistravelSimples : atividadesRegistravelSimples) {
			this.addAtividade(atividadeRegistravelSimples);
		}
	}

	public final List<PacoteDeAtividades> getPacotesQueNaoPodemSerCombinados() {
		return Collections.unmodifiableList(
				pacotesQueNaoPodemSerCombinados);
	}
	@Deprecated
	public void setPacotesQueNaoPodemSerCombinados(List<PacoteDeAtividades> pacotesQueNaoPodemSerCombinados) {
		for (PacoteDeAtividades pacoteDeAtividades : pacotesQueNaoPodemSerCombinados) {
			this.pacotesQueNaoPodemSerCombinados.add(pacoteDeAtividades);
		}
	}
	
	//Auxiliar:
	public void addAtividade(AtividadeRegistravelSimples atividade) {
		this.atividadeRegistravelSimples.add(atividade);
	}
	
	public void adicionarPacoteQueNaoPodeSerCombinado(PacoteDeAtividades pacoteQueNaoPodeSerCombinadoComOsDemais) {
		this.pacotesQueNaoPodemSerCombinados.add(pacoteQueNaoPodeSerCombinadoComOsDemais);
	}

	@Override
	public String toString() {
		return String.format("PacoteDeAtividades [id=%s, preco=%s, pacotesQueNaoPodemSerCombinados=%s]", id, preco,
				pacotesQueNaoPodemSerCombinados);
	}
}
