package com.eventos.model.activities;

import java.time.LocalDateTime;

import com.eventos.model.local.Local;

public interface Duravel extends Comparable<Duravel>{
	LocalDateTime getDataInicio();
	LocalDateTime getDataTermino();
	String getNome();
	Local getLocal();
	
	@Override
	public default int compareTo(Duravel outro) {
		if (this.getDataTermino().isBefore(outro.getDataInicio())) {
            return -1;
        }
        
		if (this.getDataInicio().isAfter(outro.getDataTermino())) {
            return 1;
        }
        
        return 0;
	}
}
