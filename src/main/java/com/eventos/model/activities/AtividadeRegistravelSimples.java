package com.eventos.model.activities;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.eventos.model.events.Evento;
import com.eventos.model.local.LocalSimples;
import com.eventos.utils.DateTimeRange;
import com.eventos.utils.LocalDateTimeAttributeConverter;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, 
property = "id")
public class AtividadeRegistravelSimples extends ComponenteRegistravelDeEvento implements Duravel{
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	protected Long id;
	@Column(nullable=false)
	private Double preco;
	@Column(nullable=false)
	private TipoAtividadeRegistravel tipoAtividadeRegistravel;
	

	@ManyToOne
	@JoinColumn(name="local_id", nullable=true)
	@JsonBackReference("atividade-local")
	private LocalSimples local;
	
	@Column
	@Convert(converter=LocalDateTimeAttributeConverter.class)
	private LocalDateTime dataInicio;
	
	@Column
	@Convert(converter=LocalDateTimeAttributeConverter.class)
	private LocalDateTime dataTermino;
	
	@ManyToMany
	@JoinTable(name = "atividade_atividade_excluida", joinColumns =  @JoinColumn(name = "atividade_id"),
    inverseJoinColumns = @JoinColumn(name = "atividade_excluida_id"))
	private List<AtividadeRegistravelSimples> atividadesQueNaoPodemFicarParalelas;
	
	@OneToMany(mappedBy="atividade")
	@JsonManagedReference("atividade-reponsavel")
	private List<Responsavel> responsaveis;
	
	@OneToMany(mappedBy="atividade")
	@JsonManagedReference("atividade-checkin")
	private List<Checkin> checkins;
	
	
	@ManyToOne
	@JoinColumn(name="evento_id", nullable=true)
	@JsonBackReference("atividade-evento")
	protected Evento evento;
	
	@ManyToOne
	@JoinColumn(name="trilha_id", nullable=true)
	@JsonBackReference("atividade-trilha")
	private Trilha trilha;
	
	@ManyToOne
	@JoinColumn(name="pacote_id", nullable=true)
	@JsonBackReference("atividade-pacote")
	private PacoteDeAtividades pacoteDeAtividades;
	
	public AtividadeRegistravelSimples(String name) {
		super(name);
		this.atividadesQueNaoPodemFicarParalelas = new ArrayList<>();
		this.preco = 0.0;
		this.responsaveis = new ArrayList<>();
	}
	
	public Double getPreco() {
		return preco;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}
	
	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public PacoteDeAtividades getPacoteDeAtividades() {
		return pacoteDeAtividades;
	}

	public void setPacoteDeAtividades(PacoteDeAtividades pacoteDeAtividades) {
		this.pacoteDeAtividades = pacoteDeAtividades;
	}

	public TipoAtividadeRegistravel getTipoAtividadeRegistravel() {
		return tipoAtividadeRegistravel;
	}

	public void setTipoAtividadeRegistravel(TipoAtividadeRegistravel tipoAtividadeRegistravel) {
		this.tipoAtividadeRegistravel = tipoAtividadeRegistravel;
	}

	public LocalDateTime getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(LocalDateTime dataInicio) {
		this.dataInicio = dataInicio;
	}

	public LocalDateTime getDataTermino() {
		return dataTermino;
	}



	public void setDataTermino(LocalDateTime dataTermino) {
		this.dataTermino = dataTermino;
	}



	public final List<Checkin> getCheckins() {
		return Collections.unmodifiableList(checkins);
	}


	@Deprecated
	public void setCheckins(List<Checkin> checkins) {
		for (Checkin checkin : checkins) {
			this.checkins.add(checkin);
		}
	}

	

	public void setPreco(double preco) {
		this.preco = preco;
	}
	
	public Double getPrice() {
		return preco;
	}

	public LocalSimples getLocal() {
		return local;
	}

	public final List<AtividadeRegistravelSimples> getAtividadesQueNaoPodemFicarParalelas() {
		return Collections.unmodifiableList(atividadesQueNaoPodemFicarParalelas);
	}

	@Deprecated
	public void setAtividadesQueNaoPodemFicarParalelas(
			List<AtividadeRegistravelSimples> atividadesQueNaoPodemFicarParalelas) {
			for (AtividadeRegistravelSimples atividadeRegistravelSimples : atividadesQueNaoPodemFicarParalelas) {
				this.adicionarAtividadeQueNaoPodeFicaParalela(atividadeRegistravelSimples);
			}
	}
	
	

	public void setLocal(LocalSimples local) {
		this.local = local;
	}

	@Override
	public String getNome() {
		// TODO Auto-generated method stub
		return this.nome;
	}

	
	public final List<Responsavel> getResponsaveis() {
		return Collections.unmodifiableList(responsaveis);
	}
	@Deprecated
	public void setResponsaveis(List<Responsavel> responsaveis) {
		for (Responsavel responsavel : responsaveis) {
			this.adicionarResponsavel(responsavel);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Trilha getTrilha() {
		return trilha;
	}

	public void setTrilha(Trilha trilha) {
		this.trilha = trilha;
	}
	
	public void adicionarCheckin(Checkin checkin) {
		this.checkins.add(checkin);
	}
	
	public void adicionarAtividadeQueNaoPodeFicaParalela(AtividadeRegistravelSimples atividadeRegistravelSimples) {
		this.atividadesQueNaoPodemFicarParalelas.add(atividadeRegistravelSimples);
	}
	
	public void adicionarResponsavel(Responsavel responsavel) {
		this.responsaveis.add(responsavel);
	}
	
	public boolean hasConflictWith (AtividadeRegistravelSimples outraAtividade) {
		
		DateTimeRange escopoDeTempoDesse = new DateTimeRange(this.dataInicio,this.dataTermino);
		DateTimeRange escopoDeTempoDoutro = new DateTimeRange(outraAtividade.dataInicio,outraAtividade.dataTermino);
		
		if (this.local.equals(outraAtividade.getLocal()) && escopoDeTempoDesse.hasConflict(escopoDeTempoDoutro)) {
			return true;
		}else {
			return false;	
		}
	
	}

	@Override
	public String toString() {
		return String.format(
				"AtividadeRegistravelSimples [id=%s, preco=%s, tipoAtividadeRegistravel=%s, dataInicio=%s, dataTermino=%s, local=%s, trilha=%s, atividadesQueNaoPodemFicarParalelas=%s, responsaveis=%s, checkins=%s]",
				id, preco, tipoAtividadeRegistravel, dataInicio, dataTermino, local, trilha,
				atividadesQueNaoPodemFicarParalelas, responsaveis, checkins);
	}
}
