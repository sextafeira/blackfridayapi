package com.eventos.model.activities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.eventos.model.events.Evento;
import com.eventos.model.local.LocalSimples;
import com.eventos.model.users.Usuario;
import com.eventos.utils.LocalDateTimeAttributeConverter;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, 
property = "id")
public class AtividadeNaoRegistravel implements Duravel{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column(nullable=false)
	private String nome;
	@Column
	private String descricao;
	@Column
	@Convert(converter=LocalDateTimeAttributeConverter.class)
	private LocalDateTime dataInicio;
	@Column
	@Convert(converter=LocalDateTimeAttributeConverter.class)
	private LocalDateTime dataTermino;
	@ManyToOne
	@JoinColumn(name="evento_id", nullable=true)
	@JsonBackReference("atividade-no-evento")
	private Evento evento;
	@ManyToOne
	@JoinColumn(name="usuario_id", nullable=true)
	@JsonBackReference("atividade-no-usuario")
	private Usuario usuarioCriador;
	
	@ManyToOne
	@JoinColumn(name="local_id", nullable=true)
	private LocalSimples local;
	
	public AtividadeNaoRegistravel(String nome) {
		this.nome = nome;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public LocalDateTime getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(LocalDateTime dataInicio) {
		this.dataInicio = dataInicio;
	}

	public LocalDateTime getDataTermino() {
		return dataTermino;
	}

	public void setDataTermino(LocalDateTime dataTermino) {
		this.dataTermino = dataTermino;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public Usuario getUsuarioCriador() {
		return usuarioCriador;
	}

	public void setUsuarioCriador(Usuario usuarioCriador) {
		this.usuarioCriador = usuarioCriador;
	}
	
	public LocalSimples getLocal() {
		return local;
	}

	public void setLocal(LocalSimples local) {
		this.local = local;
	}

	@Override
	public String toString() {
		return String.format(
				"AtividadeNaoRegistravel [id=%s, nome=%s, descricao=%s, dataInicio=%s, dataTermino=%s, evento=%s, usuarioCriador=%s]",
				id, nome, descricao, dataInicio, dataTermino, evento, usuarioCriador);
	}
}
