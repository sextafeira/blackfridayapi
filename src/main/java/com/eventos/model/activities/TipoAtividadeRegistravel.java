package com.eventos.model.activities;

public enum TipoAtividadeRegistravel {
	PALESTRA, MINICURSO, MESA_REDONDA;
}
