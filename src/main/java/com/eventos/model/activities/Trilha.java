package com.eventos.model.activities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.eventos.model.events.Evento;
import com.eventos.model.events.Tag;
import com.eventos.model.users.Usuario;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, 
property = "id")
public final class Trilha extends ComponenteRegistravelDeEvento{ //Não mais de acordo com o Diagram Class
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	private Long id;
	
	@Column(nullable=false)
	private String nome;
	
	@ManyToOne
	private Tag tag;
	
	@ManyToOne
	@JoinColumn(name="evento_id", nullable=true)
	@JsonBackReference("evento-trilha")
	protected Evento evento;
	
	@OneToMany
	private List<Usuario> usuariosCoordenadores;
	
	
	@OneToMany(mappedBy="trilha")
	@JsonManagedReference("atividade-trilha")
	private List<AtividadeRegistravelSimples> atividadeRegistravelSimples;
	
	
	public Trilha(String nome) {
		super(nome);
		this.atividadeRegistravelSimples = new ArrayList<>();
		this.usuariosCoordenadores = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Tag getTag() {
		return tag;
	}

	public void setTag(Tag tag) {
		this.tag = tag;
	}
	
	
	
	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public final List<AtividadeRegistravelSimples> getAtividadeRegistravelSimples() {
		return Collections.unmodifiableList(atividadeRegistravelSimples);
	}
	@Deprecated
	public void setAtividadeRegistravelSimples(List<AtividadeRegistravelSimples> atividadesRegistravelSimples) {
		for (AtividadeRegistravelSimples atividadeRegistravelSimples : atividadesRegistravelSimples) {
			this.adicioneAtividadeRegistravelSimples(atividadeRegistravelSimples);
		}
	}

	public final List<Usuario> getUsuariosCoordenadores() {
		return Collections.unmodifiableList(usuariosCoordenadores);
	}
	@Deprecated
	public void setUsuariosCoordenadores(List<Usuario> usuariosCoordenadores) {
		for (Usuario usuarioCoordenador : usuariosCoordenadores) {
			this.adicioneUsuarioCoordenador(usuarioCoordenador);
		}
	}
	
	public void adicioneAtividadeRegistravelSimples(AtividadeRegistravelSimples atividadeRegistravelSimples) {
		this.atividadeRegistravelSimples.add(atividadeRegistravelSimples);
	}
	
	public void adicioneUsuarioCoordenador(Usuario usuarioCoordenador) {
		this.usuariosCoordenadores.add(usuarioCoordenador);
	}

	@Override
	public String toString() {
		return String.format(
				"Trilha [id=%s, nome=%s, tag=%s, atividadeRegistravelSimples=%s, usuariosCoordenadores=%s]", id, nome,
				tag, atividadeRegistravelSimples, usuariosCoordenadores);
	}
}
