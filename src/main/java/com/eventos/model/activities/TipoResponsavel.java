package com.eventos.model.activities;

public enum TipoResponsavel {
	PALESTRANTE, INSTRUTOR, MEDIADOR;
}
