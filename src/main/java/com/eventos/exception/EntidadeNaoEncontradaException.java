package com.eventos.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code=HttpStatus.NOT_FOUND)
public class EntidadeNaoEncontradaException extends RuntimeException {
	private static final long serialVersionUID = -3443748799490192643L;

	public EntidadeNaoEncontradaException(Long id) {
		super(String.format("Nao foi possivel encontrar a entidade de ID: '%03d'", id));
	}
}