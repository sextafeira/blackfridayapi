package com.eventos.utils;

public enum MessageType
{
	SUCCESS, ERROR, ILLEGAL_ARGUMENT, INVALID_STATE
}
