package com.eventos.utils;

public interface NotificationAdapter {
	public void enviarMensagem(String destino, String mensagem);
}
