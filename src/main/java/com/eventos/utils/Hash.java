package com.eventos.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ThreadLocalRandom;

import javax.xml.bind.DatatypeConverter;

public class Hash {
	public static String getStringHash(int size){
		String hash = "";
		for(int i = 0; i < size; i++){
			int indice = ThreadLocalRandom.current().nextInt(65,90);
			hash += (char)indice;
		}
		return hash;
	}
	
	public static String getSHA1(String value){
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
		    md.update(value.getBytes());
		    byte[] digest = md.digest();
		    String myHash = DatatypeConverter
		      .printHexBinary(digest).toUpperCase();
		    return myHash;
		} catch (NoSuchAlgorithmException e) {
			return value;
		}
	}
}
