package com.eventos.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidadorGenerico {
	
	public static final String PATTERN_DATA = "dd/MM/yyyy";
	public static final String PATTERN_DATA_HORA = "dd/MM/yyyy HH:mm";
	
	public static void checkNull(Object object) {
		if(object == null)
    		throw new IllegalArgumentException("Campo nao pode ser nulo");	
	}
	
	public static void checkEmptyString(String item) {
		checkNull(item);
		if(item.isEmpty())
    		throw new IllegalArgumentException("Campo nao pode ser vazio");	
	}
	
	public static boolean checkEmail(final String hex) {
		checkEmptyString(hex);
		Pattern pattern;
		Matcher matcher;
		
		String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		
		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(hex);
		if (!matcher.matches()) {
			throw new IllegalArgumentException("Email invalido");
		}
		return matcher.matches();
	}
	
	public static boolean checkdata(final String regex) {
		checkEmptyString(regex);
		Pattern pattern;
		Matcher matcher;
		
		String DATA_PATTERN = "^(0[1-9]|[12][0-9])\\/(02)\\/([1]\\d{3}|200\\d|20\\d{2})|(0[1-9]|[12][0-9]|30)\\/(0[4,6,9]|11)\\/([1]\\d{3}|200\\d|20\\d{2})|(0[1-9]|[12][0-9]|3[01])\\/(0[13578]|1[02])\\/([1]\\d{3}|200\\d|20\\d{2})$";
		
		pattern = Pattern.compile(DATA_PATTERN);
		matcher = pattern.matcher(regex);
		if (!matcher.matches()) {
			throw new IllegalArgumentException("Data Invalida");
		}
		return matcher.matches();
	}
	
	public static boolean checkUrl(final String regex) {
		checkEmptyString(regex);
		Pattern pattern;
		Matcher matcher;
		
		String DATA_PATTERN = "^(http[s]?:\\/\\/[\\w\\/\\-\\.\\_\\?\\#]*)$";
		
		pattern = Pattern.compile(DATA_PATTERN);
		matcher = pattern.matcher(regex);
		if (!matcher.matches()) {
			throw new IllegalArgumentException("Url Invalida");
		}
		return matcher.matches();
	}
}
