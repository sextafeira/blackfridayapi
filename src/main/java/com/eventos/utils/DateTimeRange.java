package com.eventos.utils;

import java.time.LocalDateTime;

public class DateTimeRange implements Comparable<DateTimeRange>{
	private LocalDateTime dataInicio;
	private LocalDateTime dataTermino;
	
	
	
	private DateTimeRange() {
		this.dataInicio = LocalDateTime.now();
		this.dataTermino = LocalDateTime.MAX;
	}
	
	public DateTimeRange(LocalDateTime dataInicio) {
		this();
		this.dataInicio = dataInicio;
	}
	public int getHoursInterval() {
		return this.dataTermino.getHour()  - this.dataInicio.getHour();
	}
	
	public int getMinutesInterval() {
		return this.dataTermino.getMinute() - this.dataInicio.getMinute();
	}
	
	public DateTimeRange(LocalDateTime dataInicio, LocalDateTime dataTermino) {		
		this(dataInicio);
		this.dataTermino = dataTermino;
	}

	public LocalDateTime getDataInicio() {
		return dataInicio;
	}

	public LocalDateTime getDataTermino() {
		return dataTermino;
	}

	public boolean hasConflict(DateTimeRange dtr) {
		if(this.dataTermino.isBefore(dtr.dataInicio) || this.dataTermino.equals(dtr.dataInicio)) {
			return false;
		}
		
		if(dtr.dataTermino.isBefore(this.dataInicio) || dtr.dataTermino.equals(this.dataInicio)) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		if(this.dataTermino.equals(LocalDateTime.MAX))
			return String.format("{\"%s\",\"INDETERMINADO\"}", dataInicio);
		else
			return String.format("{\"%s\",\"%s\"}", dataInicio, dataTermino);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataInicio == null) ? 0 : dataInicio.hashCode());
		result = prime * result + ((dataTermino == null) ? 0 : dataTermino.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DateTimeRange other = (DateTimeRange) obj;
		if (dataInicio == null) {
			if (other.dataInicio != null)
				return false;
		} else if (!dataInicio.equals(other.dataInicio))
			return false;
		if (dataTermino == null) {
			if (other.dataTermino != null)
				return false;
		} else if (!dataTermino.equals(other.dataTermino))
			return false;
		return true;
	}

	@Override
	public int compareTo(DateTimeRange dateTimeRange) {
		if(this.dataInicio.equals(dateTimeRange.dataInicio)) {
			return 0;  // Both Are Equals
		}else if(this.dataInicio.isBefore(dateTimeRange.dataInicio)) { //IF a stars before b starts 
			return -1; //
		}else {
			return 1; //
		}
	}
}
